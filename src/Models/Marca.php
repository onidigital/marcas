<?php

namespace Onicmspack\Marcas\Models;

use Illuminate\Database\Eloquent\Model;

class Marca extends Model
{
    //
    protected $fillable = [
                           'nome',
                           'imagem',
                           'status',
    ];

    // Para retornar o arquivo do slide:
    public function arquivo()
    {
        return $this->belongsTo('Onicmspack\Arquivos\Models\Arquivo', 'imagem');
    }
}