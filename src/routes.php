<?php

Route::group(['middleware' => ['web','auth', 'authorization'], 'prefix' => 'admin' ], function () {
    // Menu do slide:
    Route::resource('marcas', 'Onicmspack\Marcas\MarcasController');
    Route::post('marcas/{id}/atualizar_status','Onicmspack\Marcas\MarcasController@atualizar_status'); // Atualizar Status Ajax
});