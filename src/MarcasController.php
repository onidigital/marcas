<?php
namespace Onicmspack\Marcas;

use Onicmspack\Marcas\Models\Marca;
use Onicmspack\Arquivos\Models\Arquivo as Arquivo;
use Onicmspack\Marcas\Requests\MarcasRequest as MarcasRequest;
use Onicms\Http\Controllers\Controller;

class MarcasController extends Controller
{
    public $caminho = 'admin/marcas/';
    public $views   = 'admin/vendor/marcas/';
    public $titulo  = 'marcas';

    public function index()
    {
        $registros = Marca::with('arquivo')->get();
        $registros = configurar_status_toogle($registros, $this->caminho);
        return view($this->views.'.index',['registros'=>$registros],[
                    'titulo' => $this->titulo,
                    'caminho' => $this->caminho,
               ]);
    }

    public function create()
    {
        $html_toggle = gerar_status_toggle( array('status' => 1) );
        return view($this->views.'.form',[
                    'titulo' => $this->titulo,
                    'caminho' => $this->caminho,
                    'html_toggle' => $html_toggle,
               ]);
    }

    public function store(MarcasRequest $request)
    {
        $input = $request->all();
        $input['imagem'] = $this->upload($input);
        Marca::create($input);

        $request->session()->flash('alert-success', config('mensagens.registro_inserido'));
        return redirect($this->caminho.'create');
    }

    public function show($id)
    {
        $registro = Marca::find($id);
        $html_toggle = gerar_status_toggle( $registro );
        return view($this->views.'.form', compact('registro'),[
                    'titulo' => $this->titulo,
                    'caminho' => $this->caminho,
                    'html_toggle' => $html_toggle,
               ]);
    }

    public function update(MarcasRequest $request, $id)
    {
        $input = $request->all();

        if(isset($input['remover_arquivo'])){
            foreach($input['remover_arquivo'] as $arq)
                $input[$arq] = null;
        }

        // se mudou o arquivo:
        if(isset($input['imagem']))
            $input['imagem'] = $this->upload($input);

        $update = Marca::find($id)->update($input);

        $request->session()->flash('alert-success', config('mensagens.registro_alterado'));
        return redirect($this->caminho.$id.'');
    }

    public function destroy($id)
    {
        Marca::find($id)->delete();
        return redirect($this->caminho);
    }

    public function upload($input)
    {
        if(!empty($input['imagem'])){
            $manipulador = new Arquivo;
            $file = $manipulador->add($input['imagem']);
            // recortar?
            $manipulador->recortar('marcas', 'imagem', $file->id);
            return $file->id;
        }
        return false;
    }

    // Atualiza um campo boolean de um registro via ajax
    public function atualizar_status($id, $coluna = 'status')
    {
        // Verifica o status atual e dá um update com o novo status:
        $registro = Marca::find($id);
        // Se encontrou o registro:
        if(isset($registro->{$coluna})){
            $novo = !$registro->{$coluna};
            $update = Marca::find($id)->update( array( $coluna =>$novo ) );
            $resposta['success'] = 'success';
            $resposta['status']  = '200';
        }else{
            $resposta['success'] = 'fail';
            $resposta['status']  = '0';
        }
        return \Response::json($resposta);
    }

}
